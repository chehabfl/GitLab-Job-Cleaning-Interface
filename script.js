const API_URL = "https://gitlab.utc.fr/api/v4/";

var fetchData = {
    method: 'GET',
    headers: new Headers()
};

var deleteData = {
    method: 'POST',
    headers: new Headers()
};

function check_response(data, callback) {
    if (data.ok) {
        data.json().then(function (json_response) {
            callback(json_response);
        });
    } else {
        alert("Une erreur est survenue lors du chargement des données. F12 pour en savoir plus.");
    }
}

var API_TOKEN;

document.getElementById("form-submit-token").onclick = function () {
    API_TOKEN = document.getElementById("form-token").value;
    fetchData.headers = new Headers({ 'PRIVATE-TOKEN': API_TOKEN });
    deleteData.headers = new Headers({ 'PRIVATE-TOKEN': API_TOKEN });
    get_projects();
};

function set_projects_list(new_html) {
    document.getElementById("project-list").innerHTML = new_html;
}

function set_jobs_list(new_html) {
    document.getElementById("jobs-list").innerHTML = new_html;
}

function get_projects() {
    set_projects_list("");
    set_jobs_list("");
    let url = API_URL + 'projects?owned=true';
    fetch(url, fetchData)
        .then(function (data) {
            check_response(data, function (projects) {
                var new_html = "<form>";
                for (var key in projects) {
                    var project = projects[key];
                    let btn = '<input type="button" value="Jobs" onclick="get_jobs(' + project.id + ')">';

                    new_html += btn + "\t" + project.name + " (id : " + project.id + ")<br><br>\n";
                }
                new_html += "</form>";
                set_projects_list(new_html);
            })
        });
}

function get_jobs(project_id) {
    set_jobs_list("");
    let url = API_URL + "projects/" + project_id + '/jobs';
    fetch(url, fetchData)
        .then(function (data) {
            check_response(data, function (jobs) {
                var new_html = "<h3> Projet : " + project_id + " (" + jobs.length + " jobs)</h3>\n<form>";
                for (var key in jobs) {
                    var job = jobs[key];
                    if (job.artifacts_file) {
                        new_html +=  '<input type="button" value="Supprimer le job." onclick="delete_job(' + project_id + ',' + job.id + ')">';
                    } else {
                        new_html += "(pas d'artefacts) ";
                    }
                    var date = new Date(Date.parse(job.created_at)).toLocaleTimeString('fr-FR', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });
                    new_html += "\t" + job.commit.message + " (auteur : "+job.commit.author_name+" | créé : "+ date +")<br><br>\n";
                }
                new_html += "</form>";
                set_jobs_list(new_html);
                document.getElementById("jobs-list").scrollIntoView();
            });

        });
}


function delete_job(project_id, job_id) {
    let url = API_URL + "projects/" + project_id + '/jobs/' + job_id + '/erase';
    fetch(url, deleteData)
        .then(function (data) {
            check_response(data, function (response) {
                get_jobs(project_id);
                alert("job supprimé");
            });
        });
}