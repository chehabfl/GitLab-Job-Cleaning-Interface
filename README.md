[![build](/../badges/master/build.svg)](https://gitlab.utc.fr/chehabfl/GitLab-Job-Cleaning-Interface/pipelines) 

_L'objectif de ce projet et de rendre disponible une interface simple permettant de supprimer les jobs de ses projets en intégration continue : il peut ne pas être très utile de garder indéfiniment des artefacts..._

# L'interface est disponible [ici](https://chehabfl.gitlab.utc.fr/GitLab-Job-Cleaning-Interface/) !

Tout se passe via l'API de GitLab. Aucun autre service n'est employé.


Pour récupérer votre token, rendez-vous [ici](https://gitlab.utc.fr/profile/personal_access_tokens).